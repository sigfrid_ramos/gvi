## Pasos para ejecutar el proyecto

1. Descargar **node js** [node](https://nodejs.org/es/).
2. Instalar el cliente de angular con npm en la terminal: **npm install -g @angular/cli**.
3. Dentro del root de la carpeta del proyecto ejecutar el comando: **npm install**.
5. Correr el proyecto con el comando: **ng serve -o**

## Ver demo aquí

[http://sigfridmx.com/gvi/](http://sigfridmx.com/gvi/)
