import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-animation4',
  templateUrl: './animation4.component.html',
  styleUrls: ['./animation4.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 0.5,
        bottom: '-350px',
        position: 'absolute',
      })),
      state('closed', style({
        opacity: 1,
        bottom: '200px',
      })),
      transition('open => closed', [
        animate('2s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ],
})
export class Animation4Component implements OnInit {

  isOpen = true;

  constructor() { }

  async ngOnInit() {
    console.log('before delay')

    await this.delay(4000);

    // Do something after
    console.log('after delay')
    this.isOpen = false;
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
