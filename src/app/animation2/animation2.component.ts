import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-animation2',
  templateUrl: './animation2.component.html',
  styleUrls: ['./animation2.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 0,
        left: '-10%',
        position: 'absolute',
      })),
      state('closed', style({
        opacity: 1,
        left: '20%',
      })),
      transition('open => closed', [
        animate('2s ease-in')
      ]),
      transition('closed => open', [
        animate('0.5s ease-in')
      ]),
    ]),
  ],
})
export class Animation2Component implements OnInit {

  isOpen = true;

  constructor() { }

  async ngOnInit() {
    console.log('before delay')

    await this.delay(2000);

    // Do something after
    console.log('after delay')
    this.isOpen = false;
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
