import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-animation1',
  templateUrl: './animation1.component.html',
  styleUrls: ['./animation1.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 0,
        right: '-200px',
        position: 'absolute',
      })),
      state('closed', style({
        opacity: 1,
        right: '300px',
      })),
      transition('open => closed', [
        animate('2s ease-in')
      ]),
      transition('closed => open', [
        animate('0.5s ease-in')
      ]),
    ]),
  ],
})
export class Animation1Component implements OnInit {

  isOpen = true;

  constructor() { }

  async ngOnInit() {
    console.log('before delay')

    await this.delay(1000);

    // Do something after
    console.log('after delay')
    this.isOpen = false;
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
