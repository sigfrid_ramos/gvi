import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-animation3',
  templateUrl: './animation3.component.html',
  styleUrls: ['./animation3.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '100px',
        opacity: 0,
        right: '-10%',
        position: 'absolute',
      })),
      state('closed', style({
        height: '100px',
        opacity: 1,
        right: '70%',
      })),
      transition('open => closed', [
        animate('2s ease-in')
      ]),
      transition('closed => open', [
        animate('0.5s ease-in')
      ]),
    ]),
    trigger('onHover', [
      // ...
      state('over', style({
        fontWeight: 'normal',
      })),
      state('leave', style({
        fontWeight: 'bold',
      })),
      transition('over => closed', [
        animate('2s')
      ]),
      transition('leave => open', [
        animate('0.5s')
      ]),
    ]),
  ],
})
export class Animation3Component implements OnInit {

  isOpen = true;
  onHover = true;

  constructor() { }

  async ngOnInit() {
    console.log('before delay')

    await this.delay(3000);

    // Do something after
    console.log('after delay')
    this.isOpen = false;
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  over() {
    console.log('Se puso el mouse sobre la imagen');
    this.onHover = false;
  }

  out() {
    console.log('Se salio el mouse sobre la imagen');
    this.onHover = true;
  }

}
